# leetcode-1 two sums

    [2,7,11,15] , 9
    need to return the index
    in this case : [0 , 1] , as arr[0](2) + arr[1](7) = 9


# 1  use hash map (which are objects in JS) to store the instance
# 2 instead of thinking 2 + ? = 9, think : can you find the result of target(9) - the current iteration(2)
# 3 loop though the array
# 4 in each iteration , set a var = target - iteration
# 5 have you seen this var in the hash map ? yes => return both of their index : no => store the var in the hash map with the var as key and the index as value; 
