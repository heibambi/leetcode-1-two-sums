const twoSum = function(nums, target) {

    let memory = {};
    for(let i = 0 ; i < nums.length ; i++){
        let left = target - nums[i];
        let found = memory[left];
    
        if(found != undefined){
            return [i , found];
        } else {
            memory[nums[i]] = i
        }
    
    }

    return null;
        
};

const answer1 = twoSum([3,2,4] , 6)
const answer2 = twoSum([3,3] , 6)
const answer3 = twoSum([2,7,11,15] , 22)

console.log({
    answer1,
    answer2,
    answer3
})